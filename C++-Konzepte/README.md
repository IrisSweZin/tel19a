# Grundlagen

In den Notebooks in diesem Ordner werden einige Grundlagen zu  `C++` und zum Programmieren im Allgemeinen gelegt.

Dies soll im Laufe der Zeit eine möglichst vollständige Einführung in C++ und ins Programmieren werden, die idealerweise auch als Folien benutzt werden kann.
Für den Anfang sind sie aber noch sehr unvollständig und ersetzen weder den Besuch der Vorlesung, noch die bisherigen Folien.

## Geplante Abschnitte:
- Ein-/Ausgabe mit `cout` und `cin`
- Variablen
- Schleifen
- If-Then-Else
- Funktionen
- Rekursion
- Arrays, `std::vector`
- Strukturen, Klassen
- C++-Programme in Quelldateien (Anleitung zum Erstellen und Compilieren)
- Überblick Compiler