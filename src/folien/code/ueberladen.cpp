#include<iostream>
#include<string>
using namespace std;

void meine_tolle_funktion(int x)
{
    cout << "Int-Version: " << x << endl; 
}

void meine_tolle_funktion(float x)
{
    cout << "Float-Version: " << x << endl; 
}

void meine_tolle_funktion(string x)
{
    cout << "String-Version: " << x << endl; 
}

int main()
{
    int x = 42; float y = 4.2; string z = "42";
    meine_tolle_funktion(x);
    meine_tolle_funktion(y);
    meine_tolle_funktion(z);
    return 0;
}
