#include<iostream>
#include<string>
#include<sstream>
using namespace std;

struct Point
{
private:
    float x;
    float y;

public:
    Point(float x_, float y_);
    void move(float dx, float dy);
    string str();
};

int main() {
    Point p{3,4};
    p.move(2.5, 1.8);
    cout << p.str() << endl; 
    
    return 0;
}

Point::Point(float x_, float y_)
    : x{x_}, y{y_}
    {}

void Point::move(float dx, float dy)
{
    x += dx;
    y += dy;
}

string Point::str()
{
    stringstream s;
    s << "(" << x << "," 
      << y << ")";
    return s.str();
}

/* Folgendes ist nicht m�glich:

int main() {
    Point p{3,4};
    p.x += 3.5;
    cout << p.str() << endl; 
    
    return 0;
}
*/

