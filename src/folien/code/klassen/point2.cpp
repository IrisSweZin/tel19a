#include<iostream>
#include<string>
#include<sstream>
using namespace std;

struct Point
{
    float x;
    float y;
    
    void move(float dx, float dy);
    string str();
};

int main() {
    Point p{3,4};
    p.move(2.5, 1.8);
    cout << p.str() << endl; 
    
    return 0;
}

void Point::move(float dx, float dy)
{
    x += dx;
    y += dy;
}

string Point::str()
{
    stringstream s;
    s << "(" << x << "," 
      << y << ")";
    return s.str();
}
