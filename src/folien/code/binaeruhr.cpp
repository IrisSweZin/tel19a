#include<ctime>
#include<string>
#include<iostream>
#include<iomanip>
using namespace std;

// Funktionen zum Bestimmen der Zeit
int get_hour(time_t t);
int get_minute(time_t t);
int get_second(time_t t);

// Konvertieren einer Zahl in einen Bin�rstring
string to_bin(int n);

int main() {
    time_t t = time(nullptr);
    
    string bin_hour = to_bin(get_hour(t));
    string bin_minute = to_bin(get_minute(t));
    string bin_second = to_bin(get_second(t));

    cout << setw(6) << bin_hour << endl;
    cout << setw(6) << bin_minute << endl;
    cout << setw(6) << bin_second << endl;

    return 0;	
}


// Implementierung
string to_binary(int n)
{
    string result;
    while(n != 0)
    {
        result = (n%2==0 ? " " : "*") + result;
        n/=2;
    }
    return result;
}

// Hilfsfunktion f�r die Zeitbestimmung:
// Wir berechnen die laufende Sekunde am aktuellen Tag.
int second_this_day(time_t t)
{
    /* Ein Tag hat 24 * 60 * 60 = 86400 Sekunden.
       Also berechnen wir die Zeit modulo 86400,
       um die laufende Sekunde dieses Tages zu bekommen.
     */
    return t % (24*60*60);
}

int get_current_hour(time_t t)
{
    return (second_this_day(t) / (60*60)) % 24;
}


int get_current_minute(time_t t)
{
    return (second_this_day(t) / 60) % 60;
}


int get_current_second(time_t t)
{
    return second_this_day(t) % 60;
}

