#include<iostream>
#include<vector>
using namespace std;

void foo(vector<int> v) // Wertparameter
{
    for (int el:v) cout << el;
}

void bar(vector<int> & v) // Referenzparameter
{
    for (int el:v) cout << el;
}

int main()
{
    // Grossen Vector erzeugen
    vector<int> vec;
    for (int i = 0; i<100000; i++) vec.push_back(i);
    
    // Beide Aufrufe haben den gleichen Effekt:    
    foo(vec);     // langsam (v wird kopiert)
    bar(vec);     // schnell (keine Kopie)
    
    return 0;
}


					
					
					
					
					
					
					
					
