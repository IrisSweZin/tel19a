#include<iostream>
#include<vector>
using namespace std;
					
int main()
{
    // Deklaration eines leeren Vektors aus Zahlen:
    vector<int> v1;
    
    // Deklaration eines Vektors mit Inhalt:
    vector<int> v2({3,4,7,42});
    vector<int> v3 = {5,-2,50,77};
    vector<int> v4(10,6);    // 10 Elemente, Inhalt 6
    
    // Anhaengen von Elementen:
    v3.push_back(103);

    // Zugriff auf einzelne Elemente:
    int x = v4[2];
    
    // Eigenschaften von Vektoren:
    bool leer = v1.empty();
    int laenge = v3.size();
    
    // Iterieren ueber Elemente:
    for (int el:v3) { cout << el << " "; }
    for (int i=0; i<v4.size(); i+=2)
    {
        cout << v4[i] << " ";
    }
    
    // Vektoren mit anderem Inhalt:
    vector<float> v5 = { 3.0, 4.2, 5};
    
    // Vektoren aus Vektoren:
    // Eine Art Tabelle:
    vector<vector<int>> v6 = {{3,4},{6,7,8}};
    
    // neue Zeile:
    v6.push_back({100,10});
    
    // neue Zahl zu Zeile 2:
    v6[1].push_back(10);
    
    return 0;
}


					
					
					
					
					
					
					
					
