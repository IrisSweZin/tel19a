#include<iostream>
#include<string>
#include"summe.h"

int main() {
    
    std::cout << "summe(3,4) == " 
              << summe(3,4) << "\n";
    std::cout << "summe(3.5,4) == " 
              << summe<double>(3.5,4) << "\n";
    std::cout << "summe(\"abc\",\"def\") == "
              << summe<std::string>("abc","def")
              << "\n";
    
    return 0;
}
