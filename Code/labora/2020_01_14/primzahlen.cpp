#include<iostream>
#include<vector>
using namespace std;

template<typename T>
void print_vector(vector<T> v) {

	for (T e : v) {
		cout << e << " ";
	}
	cout << endl;
}

bool prim(int x) {
	if (x <= 1) { return false; }
	for (int i=2; i<x; ++i) {
		if (x%i == 0) {
			return false;
		}
	}
	return true;
}


// Liefert einen Vektor mit allen Primzahlen,
// die kleiner sind als x.
vector<int> primzahlen(int x) {
	vector<int> v;
	
	for (int i= 2; i<x; i++) {
		if (prim(i)) {
			v.push_back(i);
		}
	}
	return v;
}


int main() {
	
	cout << prim(3) << endl; // Soll 1 ausgeben
	cout << prim(5) << endl; // Soll 1 ausgeben
	cout << prim(4) << endl; // Soll 0 ausgeben
	cout << prim(1) << endl; // Soll 0 ausgeben
	
	print_vector(primzahlen(15));
	
	return 0;
}