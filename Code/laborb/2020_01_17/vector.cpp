#include<iostream>
#include<vector>
#include<string>
using namespace std;

template<typename T>
void print_vector(vector<T> v) {
	for (T e : v) {
		cout << e << " ";
	}
	cout << endl;
}


// void print_vector(vector<int> v) {
	// for (auto e : v) {
		// cout << e;
	// }
// /*	
	// for (int i=0; i<v.size(); i++) {
		// int e = v[i];
		// cout << e;
	// }
// */
// /*
	// for (int i=0; i<v.size(); i++) {
		// cout << v[i] << " ";
	// }
// */
	// cout << endl;
// }

int main() {
	int x = 42;
	vector<int> a = { 4, 3, 6, 42, 38 };
	
	cout << a[4] << endl;
	cout << a.size() << endl;
	a.push_back(103);
	cout << a[5] << endl;
	cout << a.size() << endl;
	
	print_vector(a);
	
	
	
	
	
	vector<string> v2 = {"Hallo", "Zweiundvierzig"};
	print_vector(v2);
	
	
	
}

