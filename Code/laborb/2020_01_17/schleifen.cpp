#include<iostream>
using namespace std;

int main() {
// Wir berechnen die Summe der Zahlen von 1 bis 10

	
	int ergebnis = 0;
	
// Die kurze Variante
	for (int i=1; i<=10; i=i+1) {
		ergebnis = ergebnis + i;
	}

// Die lange Variante
/*
	ergebnis = ergebnis + 1;
	ergebnis = ergebnis + 2;
	ergebnis = ergebnis + 3;
	ergebnis = ergebnis + 4;
	ergebnis = ergebnis + 5;
	ergebnis = ergebnis + 6;
	ergebnis = ergebnis + 7;
	ergebnis = ergebnis + 8;
	ergebnis = ergebnis + 9;
	ergebnis = ergebnis + 10;
*/
	
	cout << ergebnis;
	
	return 0;
}