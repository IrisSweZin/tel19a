#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Suchen von Elementen in einem Vektor ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `find()`, die als Argument einen Vektor `v` aus Zahlen und eine einzelne Zahl `x` erwartet.
Die Funktion soll die Position von `x` in `v` bestimmen und zurückgeben. Kommt `x` nicht vor, soll die Länge von `v` geliefert werden.
***/
int find(vector<int> v, int x) {
    // ...
    return 0;
}

/*** TESTCODE/MAIN ***/
int main()
{
    vector<int> v1 = { 1, 3, 5, 5, 7, 9, 11 }
    
    cout << find_two(v1, 5) << endl;   // Soll '2' ausgeben.
    cout << find_two(v1, 1) << endl;   // Soll '7' ausgeben.
    cout << find_two(v1, 42) << endl;  // Soll '7' ausgeben.
}